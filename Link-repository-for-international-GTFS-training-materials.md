Welcome to the GTFS-Training-Materials wiki!

This is a staging ground for building a repository of links to unofficial, multi-lingual GTFS training materials that are free for anyone to use. Feedback, contributions, and edits to this wiki are welcome!


**NEW FREE ON-LINE GTFS COURSE!**

The World Bank is offering a free on-line course on GTFS. The course objective is to empower participants to create, manage, and use GTFS feeds, particularly in cities that do not already have complete electronic systems for storing and managing their transit system data. The course methodologies are based upon the World Bank and others’ successful experiences in creating GTFS feeds in resource-constrained environments and reflect the latest work and knowledge in open data and open-source software.

https://olc.worldbank.org/content/introduction-general-transit-feed-specification-gtfs-and-informal-transit-system-mapping 

**GTFS Data Management**

*Unofficial Translations of GTFS Standard Reference Materials*
* // Source: https://developers.google.com/transit/gtfs/reference
* Chinese: https://drive.google.com/file/d/0B5Ot4FPs1rW6WW9rdkxtQVlJQ2s/edit?usp=sharing
* Vietnamese: https://drive.google.com/file/d/0B5Ot4FPs1rW6MVNqNzdhcnhrUjA/edit?usp=sharing

*GTFS Examples*
* Chinese: https://drive.google.com/file/d/0B5Ot4FPs1rW6SWRYOXB3Q1ZKZkU/edit?usp=sharing
* English: https://docs.google.com/document/d/16inL5BVcM1aU-_DcFJay_tC6Ni0wPa0nvQEstueG5k4/edit?usp=sharing

*GTFS Validation Guidance*
* // Source: https://code.google.com/p/googletransitdatafeed 
* Chinese: https://drive.google.com/file/d/0B5Ot4FPs1rW6MXlhaEZHNnZvZkU/edit?usp=sharing

*Pre-Processing GTFS Data Collected Using TransitWand*
* // Referenced open source tool: https://github.com/conveyal/transit-wand
* English: https://drive.google.com/file/d/0B5Ot4FPs1rW6NDh2OEEzUEVfYTA/edit?usp=sharing

*GTFS Background On-Line Resources*
* Chinese: https://drive.google.com/file/d/0B5Ot4FPs1rW6LTBWU1JmNnVVX28/edit?usp=sharing
* English: https://drive.google.com/file/d/0B5Ot4FPs1rW6Z1VQY1JFaC1QSk0/edit?usp=sharing

*GTFS County-Specific Issues*
* China: https://drive.google.com/file/d/0B5Ot4FPs1rW6S09rMUE3VlpIcTQ/edit?usp=sharing

*GTFS Transit Indicator Calculation SQL Code*
* SQL code for calculating about 30 indicators from GTFS data, prepared by Azavea: https://drive.google.com/file/d/0B5Ot4FPs1rW6TDV2bWNuRVFZNTA/view?usp=sharing

**Open Source Applications that Support GTFS**
* // Note: Many of these starter-list links are works-in-progress

* **Open Transit Indicators**: Web-based, very user-friendly application for generating transit service, accessibility, and performance indicators from GTFS, GTFS-RT, and GIS census data.  
https://github.com/WorldBank-Transport/open-transit-indicators

* **TransitWand**: Mobile application for collecting GTFS data in the field  
https://github.com/conveyal/transit-wand

* **GTFS Editor**: Web-based visually-based application for creating / editing a GTFS feed  
https://github.com/conveyal/gtfs-editor

* **Open Trip Planner**: Web-based multi-modal trip planning tool  
https://github.com/openplans/OpenTripPlanner

* **Open Trip Planner Analyst**: Web-based platform for using GTFS data to analyze transit service data  
https://github.com/openplans/opentripplanner-analyst

* **GTFS RT Admin Tool**: Web-based platform for tracking transit service delays  
https://github.com/conveyal/gtfs-rt-admin

* **Sakay**: Web and SMS platform for trip planning that supports informal transit routes  
https://github.com/thatsmydoing/sakay-gateway  
https://github.com/ahelpingchip/sakayph

* **SimpleGTFSCreator**: Python script that generates GTFS feeds from GIS files  
https://github.com/PatSunter/SimpleGTFSCreator

* **Inovatica TransitArt**: Three modules for generating interactive web-based schedules
https://github.com/inovatica/TransitArtTypo3Extension  
https://github.com/inovatica/TransitArtWordpressPlugin  
https://github.com/inovatica/TransitArtJoomlaComponent  
https://github.com/inovatica/TransitArtLiferayPortlet

**Other Resources**

*Transit Land*
* Crowd-sourced directory of official and unofficial GTFS feeds from across the world -- very fancy! 
* https://transit.land/

*Transit Feeds*
* Worldwide directory of GTFS and GTFS-realtime feeds.  Data is stored in github.
* http://transitfeeds.com

*Headway Blog by Joe Hughes (no longer updated)*
* http://web.archive.org/web/20121010014725/http://headwayblog.com/wiki/index.php?title=Main_Page




**Contributors**
* Aaron Antrim, [Trillium Solutions](http://trilliumtransit.com); aaron@trilliumtransit.com. Documents worked on: GTFS Examples, GTFS On-Line Resources, GTFS Validation Guidance
* Li Qu, World Bank; lqu@worldbank.org. Documents worked on: Pre-Processing GTFS Data Collected Using TransitWand
* Holly Krambeck, World Bank; hkrambeck@worldbank.org 